/**
 * Created by lievin on 12/30/2021.
 */
$(document).ready(function () {
    data_words = words
    var colorArray=["#019875","#1E8BC3","#D91E18","#D35400","#8E44AD","#C0392B"];
    var cardState;
    var currentQuestion=0;
    var qbank=new Array;
    var fail =new Array;
    document.getElementById("rightanswer").textContent = 0
    document.getElementById("totalcards").textContent = data_words.length 
    loadDB(data_words);
   
    function loadDB(data){
      for(i=0;i<data.length;i++){
       qbank[i]=[];
       qbank[i][0]=data[i][0];
       qbank[i][1]=data[i][1];
       qbank[i][2]=data[i][2];
      }//for
      beginActivity();
    }//loadDB
   
    function beginActivity(){
     cardState=0;
    //  var color1=colorArray[Math.floor(Math.random()*colorArray.length)];
     $("#cardArea").empty();
     $("#cardArea").append('<div id="card1" class="card">' + qbank[currentQuestion][0] + '</div>');
     $("#cardArea").append('<div id="card2" class="card"><span>' + qbank[currentQuestion][1] + '</span><span>' + qbank[currentQuestion][2] + '</span></div>');
     $("#card1").css("background-color","#A266E6");
     $("#card2").css("background-color","#C0C0C0");
     $("#cardArea").on("click",function(){
      if(cardState!=1){
       cardState=1;
       
       //togglePosition();
       $("#card1").animate({top: "-=200"}, 400, function() {cardState=0;togglePosition();});
       $("#card2").animate({top: "-=200"}, 400, function() {togglePosition2();});
      }
     });//click function
     currentQuestion++;
     $("#buttonLine").empty();
     $("#buttonLine").append('<div class="btnFlash col-4" id="notYet">Not yet</div>');
     $("#buttonLine").append('<div class="btnFlash col-4" id="remember">Remember</div>');
     $("#remember").on("click",function(){
        currentNanswer = parseInt(document.getElementById("rightanswer").textContent) + 1
        document.getElementById("rightanswer").textContent = currentNanswer
        if(currentQuestion<qbank.length){beginActivity();}
        else{
            if (fail.length > 0) {
                qbank = fail
                fail = new Array;
                currentQuestion = 0
                beginActivity();
            } else {
                displayFinalMessage();
            }
        }
       });
     $("#notYet").on("click",function(){
        fail.push(qbank[currentQuestion-1])
        if(currentQuestion<qbank.length){beginActivity();}
        else{
            if (fail.length > 0) {
                qbank = fail
                fail = new Array;
                currentQuestion = 0
                beginActivity();
            } else {
                displayFinalMessage();
            }
        }
       });
    }//beginactivity
   
    function togglePosition(){
     if($("#card1").position().top<=-150){
        $("#card1").css("top","200px");
     };
    }//toggle
   
    function togglePosition2(){
      if($("#card2").position().top<=-150){
          $("#card2").css("top","0px");};
    }//toggle2
   
    function displayFinalMessage(){
     $("#buttonLine").empty();
     $("#cardArea").empty();
     $("#cardArea").append('<div id="finalMessage">You have finished the activity.</div>');
    }//final message
   
});