/**
 * Created by lievin on 12/29/2021.
 */
$(document).ready(function(){
    $( "#dataWords" ).val(JSON.stringify(words));
    $( "#ndataWords" ).val(words.length);
    $(document).ready(function() {
        $('#example').DataTable( {
            data: words,
            pageLength: 10,
            lengthMenu: [[10, 25, 50,100,200,500, -1], [10, 25, 50,100,200,500, "All"]],
            columns: [
                { title: "Word" },
                { title: "Pinyin" },
                { title: "English" },
                { title: "Level" },
                { title: "Occurrence" }
            ]
        } );
    } );


    var handle = $( "#custom-handle" );
    $( "#slider" ).slider({
        min:1,
        max:words.length,
        value: words.length,
        create: function() {
            handle.text( $( this ).slider( "value" ) );
        },
        slide: function( event, ui ) {
            // $( "#dataWords" ).val(JSON.stringify(words));
            handle.text( ui.value );
        },
        stop: function( event, ui ) {
            document.getElementById("analyseButton").textContent = "Play "+ ui.value +" flashcards"
            $( "#ndataWords" ).val(ui.value);
        }
    });
    if (basic_info["size"]>0) {
        document.getElementById("percent_total").textContent = Math.floor(basic_info["number_of_words"]/basic_info["size"]*100)
        document.getElementById("percent_1").textContent = Math.floor(level_split["1"]/basic_info["size"]*100)
        document.getElementById("percent_2").textContent = Math.floor(level_split["2"]/basic_info["size"]*100)
        document.getElementById("percent_3").textContent = Math.floor(level_split["3"]/basic_info["size"]*100)
        document.getElementById("percent_4").textContent = Math.floor(level_split["4"]/basic_info["size"]*100)
        document.getElementById("percent_5").textContent = Math.floor(level_split["5"]/basic_info["size"]*100)
        document.getElementById("percent_6").textContent = Math.floor(level_split["6"]/basic_info["size"]*100)
        document.getElementById("percent_leftover").textContent = Math.floor(level_split["7"]/basic_info["size"]*100)
    } else {
        document.getElementById("percent_total").textContent = 100
        document.getElementById("percent_1").textContent = 100
        document.getElementById("percent_2").textContent = 100
        document.getElementById("percent_3").textContent = 100
        document.getElementById("percent_4").textContent = 100
        document.getElementById("percent_5").textContent = 100
        document.getElementById("percent_6").textContent = 100
        document.getElementById("percent_leftover").textContent = 100
    }
    
})