var text = ""
var result_data = null;

$(document).ready(function(){
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;
    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
            var dropdownContent = this.nextElementSibling;
            console.log(dropdownContent.style.display)
            if (this.classList.contains("active")) {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";
            }
            this.classList.toggle("active");
        });
    }
})

function loading() {
    el = document.getElementById("loading")
    el.style.position = "absolute"
    el.style.visibility = "visible"
    el.style.top = "50%"
    el.style.left = "50%"
    el.style.margin = "-100px 0 0 -100px"
}

function unloading() {
    document.getElementById("loading").style.visibility = "hidden";
}


// function analyse() {
//     $('#analyseButton').attr('disabled', 'disabled');
//     loading()
//     difficulty_minimum = $( "#slider-range" ).slider( "values", 0 )
//     difficulty_maximum = $( "#slider-range" ).slider( "values", 1 )
//     number_occurrences_minimum = $( "#slider-range" ).slider( "value" )
//     new_text = document.getElementById("origintext").value
//     text = new_text
//     datas = {
//         "text":new_text,
//         "number_occurrences_minimum":number_occurrences_minimum,
//         "difficulty_minimum":difficulty_minimum,
//         "difficulty_maximum":difficulty_maximum
//     }
//     // rest_call("localhost:5052",datas)
// }

function rest_call(host, datas) {
    $.ajax({
        type: 'POST',
        url: 'http://'+host+'/requests/monitoring',
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        data: JSON.stringify(datas),
        async: true,
        success: function(data) {
            result_data = data
            unloading()
            // window.location.replace('http://'+host+'/requests/analyse');
            $().redirect('http://'+host+'/requests/analyse', data);
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            text = ""
            unloading()
            alert("monitoring did not work");
        }
    });
}