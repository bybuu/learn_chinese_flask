$(document).ready(function(){
    empty = $('#origintext').val().length == 0
    if (empty)
        $('#analyseButton').attr('disabled', 'disabled');
    else
        $('#analyseButton').attr('disabled', false);
    $('#origintext').on('keyup', function() {
        let empty = false;
        empty = $(this).val().length == 0;
    
        if (empty)
            $('#analyseButton').attr('disabled', 'disabled');
        else
            $('#analyseButton').attr('disabled', false);
        });
    $('#analyseForm').submit(function(){
        $("#analyseButton").attr("disabled", true);
        loading()
    });
  $( "#difficulty_minimum" ).val(1);
  $( "#difficulty_maximum" ).val(6);
  $( "#number_occurrences_minimum" ).val(1);
    $( "#slider-range" ).slider({
      range: true,
      min: 1,
      max: 6,
      values: [ 1, 7 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "LEVEL " + ui.values[ 0 ] + " TO LEVEL " + ui.values[ 1 ] );
        $( "#difficulty_minimum" ).val(ui.values[ 0 ]);
        $( "#difficulty_maximum" ).val(ui.values[ 1 ]);
      }
    });

    $( "#amount" ).val( "LEVEL " + $( "#slider-range" ).slider( "values", 0 ) +
      " TO LEVEL" + $( "#slider-range" ).slider( "values", 1 ) );

    var handle = $( "#custom-handle" );
    $( "#slider" ).slider({
         min:1,
          create: function() {
            handle.text( $( this ).slider( "value" ) );
          },
          slide: function( event, ui ) {
            handle.text( ui.value );
            $( "#number_occurrences_minimum" ).val(ui.value);
          }
    });
    document.getElementById("loading").style.visibility = "hidden";
})