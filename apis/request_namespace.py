# from flask import make_response, jsonify, g,render_template, request
# from flask_cors import cross_origin
# from lib.test_jieba import  analyze_text, reset_cache
# import logging
# from app import app
# import time
# api = Namespace("requests", description="Requests related operations")
#
# analyze_post = {
#     "data": {},
#     "response_code": fields.String,
#     "message": fields.String,
# }
#
#
# # class Requests(Resource):
#
#
#
#
# @api.route("/monitoring")
# class Requests(Resource):
#     """Class that deals with individual existing resources."""
#
#     post_analyze_parser = reqparse.RequestParser(bundle_errors=True)
#     post_analyze_parser.add_argument("text", type=str)
#     post_analyze_parser.add_argument("number_occurrences_minimum", type=int)
#     post_analyze_parser.add_argument("difficulty_minimum", type=int)
#     post_analyze_parser.add_argument("difficulty_maximum", type=int)
#     @api.response(200, "Success", analyze_post)
#     @api.response(404, "Request Not Found")
#     @cross_origin()
#     def post(self):
#         logging.info("Post content")
#         data = self.post_analyze_parser.parse_args()
#         object_returned = analyze_text(data["text"],data["number_occurrences_minimum"],data["difficulty_minimum"],data["difficulty_maximum"])
#         print(object_returned)
#         print(object_returned["basic_info"])
#         print(object_returned["words"])
#         print(object_returned["level_split"])
#         test = jsonify({"data":object_returned,"response_code":"200","message":"success"})
#         return make_response(test,"200")
#
#
# @api.route("/resetcache")
# class Requests(Resource):
#     """Class that deals with individual existing resources."""
#
#     @api.response(200, "Success")
#     @api.response(404, "Request Not Found")
#     @cross_origin()
#     def get(self):
#         logging.info("Get cache")
#         reset_cache()
#         return make_response("success","200")