# learn_chinese_flask

## Purpose
```
DianDianLaoshi is a tool to help Chinese students and Chinese teachers to reach their goal by giving more information about a Chinese text (HSK or TOCFL level and the most used words in the text).
You can challenge your Mandarin characters skills by playing Chinese flashcards.
```

## Getting started
```
python3.10
git clone https://gitlab.com/bybuu/learn_chinese_flask.git
cd existing_repo
python -m venv venv
source venv/bin/activate
python app.py

Reach from your favorit browser this url:
http://localhost:5052/
Put this sentance in the text box: 我的名字是本，我喜歡足球

And click on Analyze


Or play flash cards here:
http://localhost:5052/flashcards
```
