from flask import Flask, g, render_template, request, jsonify
from lib.test_jieba import  analyze_text, reset_cache,parse_hsk_files, get_level_card, get_words_info, get_cache, write_file, set_increment, get_monitoring
from lib import logs
import ast
import random

app = Flask(__name__)

@app.errorhandler(404)
def not_found(e):
    # set_increment("notfound_page")
    return render_template('notfound.html')

@app.route('/')
def landing():
    return render_template('index.html')

@app.route('/flashcards', methods=['POST','GET'])
def flashcards():
    if request.method == 'POST':
        words = request.form.get("dataWords")
        words = ast.literal_eval(words)
        ndatawords = request.form.get("ndataWords")
    else:
        level = request.args.get("level")
        language = request.args.get("language")
        if level is None or language is None:
            words = get_level_card(1, "ZH")
        else:    
            words = get_level_card(level, language)
        ndatawords = len(words)
    words_rand = random.sample(words, k=int(ndatawords))
    return render_template('flashcards.html', words=words_rand)
    

@app.route("/analyse", methods=['POST','GET'])
def analyze():
    if request.method == 'POST':
        object_returned = analyze_text(request.form.get("text"), int(request.form.get("number_occurrences_minimum")), int(request.form.get("difficulty_minimum")),
                                    int(request.form.get("difficulty_maximum")))
        return render_template('analyse.html', **object_returned)
    else:
        level = request.args.get("level")
        language = request.args.get("language")
        if level is None or language is None:
            object_returned = get_words_info(1, "ZH")
        else:
            try:
                if int(level) < 1 or int(level) > 7:
                    level = 1
            except Exception as e:
                level = 1
            object_returned = get_words_info(int(level), str(language).upper())
        return render_template('analyse.html', **object_returned)


@app.route("/cache", methods=['GET'])
def get_cache_endpoint():
    id = request.args.get("id")
    if str(id) == "941993":
        obj = get_cache()
        return jsonify(results=obj)

@app.route("/write_file", methods=['GET'])
def write_file_endpoint():
    id = request.args.get("id")
    if str(id) == "941993":
        obj = write_file()
        return jsonify(results=obj)

@app.route("/monitoring", methods=['GET'])
def monitoring():
    id = request.args.get("id")
    if str(id) == "941993":
        return jsonify(results=get_monitoring())

if __name__ == '__main__':
    logs.logger_()
    app.run(host='0.0.0.0',port=5052)

