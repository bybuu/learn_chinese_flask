import logging
from datetime import date
from pathlib import Path

import tarfile
import pathlib
import os
import glob

def logger_():
    """
    Logs information at three levels: debug, error, info. At the end of each day, converts to tar.gz file
    """
    today = date.today()
    today_ = today.strftime("%m-%d-%y")
    paths = glob.glob("../logs/messages.debug.*")
    if len(paths) > 7:
        do_tar = True
        prev_day = paths[0][-8:]
    else:
        do_tar = False
    my_file = Path("../logs/messages.debug." + today_)
    if not my_file.is_file():
        # Tar the old logging files
        log_dir = pathlib.Path("..") / "logs"
        output_dir = pathlib.Path("..") / "history_logs"

        targets = list(log_dir.glob("messages.*"))
        if do_tar:
            name = "logs." + prev_day + ".tar.gz"
            output_file = output_dir / name

            with output_file.open("wb") as ofile:
                with tarfile.open(fileobj=ofile, mode="w:gz") as tarball:
                    for path in targets:
                        tarball.add(path.resolve(), path.name)

        # Deletes the old logging files
            files = glob.glob('../logs/*')
            for f in files:
                os.remove(f)

        # Create new logging files
        dir_path = os.path.dirname(os.path.realpath(__file__))
        logs_path = os.path.join(dir_path, "..")
        with open(os.path.join(logs_path, "logs/messages.debug." + today_), "w") as fp:
            pass
        with open(os.path.join(logs_path, "logs/messages.error." + today_), 'w') as fp:
            pass
        with open(os.path.join(logs_path, "logs/messages.info." + today_), 'w') as fp:
            pass

    logger = logging.getLogger()
    formatter = logging.Formatter("%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s")
    fh1 = logging.FileHandler(os.path.join(logs_path, "logs/messages.debug." + today_), "a")
    fh2 = logging.FileHandler(os.path.join(logs_path, "logs/messages.error." + today_), "a")
    fh3 = logging.FileHandler(os.path.join(logs_path, "logs/messages.info." + today_), "a")
    fh1.setLevel(logging.DEBUG)
    fh2.setLevel(logging.ERROR)
    fh3.setLevel(logging.INFO)
    fh1.setFormatter(formatter)
    fh2.setFormatter(formatter)
    fh3.setFormatter(formatter)
    logger.addHandler(fh1)
    logger.addHandler(fh2)
    logger.addHandler(fh3)
    logger.setLevel(logging.DEBUG)