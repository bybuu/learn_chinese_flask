import jieba
import hanzidentifier
from googletrans import Translator
import pinyin

tw_dict = {}
zh_dict = {}
monitoring_dict = {}
leftover_zh = set()
leftover_tw = set()


def set_increment(page):
    global monitoring_dict
    if page == "homepage_page":
        if "homepage_page" in monitoring_dict:
            monitoring_dict["homepage_page"] = monitoring_dict["homepage_page"] + 1
        else:
            monitoring_dict["homepage_page"] = 1
    elif page == "analyse_page":
        if "analyse_page" in monitoring_dict:
            monitoring_dict["analyse_page"] = monitoring_dict["analyse_page"] + 1
        else:
            monitoring_dict["analyse_page"] = 1
    elif page == "flashcards_page":
        if "flashcards_page" in monitoring_dict:
            monitoring_dict["flashcards_page"] = monitoring_dict["flashcards_page"] + 1
        else:
            monitoring_dict["flashcards_page"] = 1
    elif page == "notfound_page":
        if "notfound_page" in monitoring_dict:
            monitoring_dict["notfound_page"] = monitoring_dict["notfound_page"] + 1
        else:
            monitoring_dict["notfound_page"] = 1

def get_monitoring():
    global monitoring_dict
    return monitoring_dict


def get_level_card(level,chinese_type):
    global tw_dict
    global zh_dict
    if chinese_type == "TW":
        current_dict = tw_dict
    else:
        current_dict = zh_dict
    response_card = []
    level_dict = current_dict[int(level)]
    for word in current_dict[int(level)]:
        ll_word = [word, level_dict[word][0], level_dict[word][1], level, 1]
        response_card.append(ll_word)
    return response_card

def get_words_info(level,chinese_type):
    words = get_level_card(level,chinese_type)
    return_object = {
        "basic_info": {
            "filter":{
                "number_occurrences_minimum": 1,
                "difficulty_minimum":level,
                "difficulty_maximum":level
            },
            "size":len(words),
            "covered":len(words),
            "number_of_words":len(words),
        },
        "level_split":{
            "1": 0,
            "2": 0,
            "3": 0,
            "4": 0,
            "5": 0,
            "6": 0,
            "7": 0
        },
        "words": words
    }
    return_object["level_split"][str(level)] = len(words)
    return return_object

def parse_hsk_files():
    text5 = ""
    text6 = ""
    leftover = ""
    with open("chinese_words/level5", encoding="utf-8") as file:
        val = file.read()
    level5ss = val.split("\n")

    with open("chinese_words/level6", encoding="utf-8") as file:
        val = file.read()
    level6ss = val.split("\n")

    with open("chinese_words/level11", encoding="utf-8") as file:
        val = file.read()
    level56s = val.split("\n")
    dict56 = {}
    for line in level56s:
        ll = line.split("|")
        if ll[0] in level5ss:
            text5 += line + '\n'
        elif ll[0] in level6ss:
            text6 += line + '\n'
        else:
            leftover += line + '\n'
    try:
        with open("chinese_words/level5bis", "w", encoding="utf-8") as file:
            file.write(text5)
    except Exception as e:
        pass
        # print(e)
    with open("chinese_words/level6bis", "w", encoding="utf-8") as file:
        file.write(text6)
    with open("chinese_words/leftover", "w", encoding="utf-8") as file:
        file.write(leftover)

def set_hsk_files():
    hsk1s = {}
    hsk2s = {}
    hsk3s = {}
    hsk4s = {}
    hsk5s = {}
    hsk6s = {}
    hsk7s = {}
    global zh_dict

    with open("hsk/hsk1", encoding="utf-8") as file:
        val = file.read()
    ll1s = val.split("\n")
    for ll1 in ll1s:
        parse_1 = ll1.split("|")
        hsk1s[parse_1[0]] = [parse_1[1],parse_1[2]]
    zh_dict[1] = hsk1s

    with open("hsk/hsk2", encoding="utf-8") as file:
        val = file.read()
    ll2s = val.split("\n")
    for ll2 in ll2s:
        parse_2 = ll2.split("|")
        hsk2s[parse_2[0]] = [parse_2[1],parse_2[2]]
    zh_dict[2] = hsk2s

    with open("hsk/hsk3", encoding="utf-8") as file:
        val = file.read()
    ll3s = val.split("\n")
    for ll3 in ll3s:
        parse_3 = ll3.split("|")
        hsk3s[parse_3[0]] = [parse_3[1],parse_3[2]]
    zh_dict[3] = hsk3s

    with open("hsk/hsk4", encoding="utf-8") as file:
        val = file.read()
    ll4s = val.split("\n")
    for ll4 in ll4s:
        parse_4 = ll4.split("|")
        hsk4s[parse_4[0]] = [parse_4[1],parse_4[2]]
    zh_dict[4] = hsk4s    
    
    with open("hsk/hsk5", encoding="utf-8") as file:
        val = file.read()
    ll5s = val.split("\n")
    for ll5 in ll5s:
        parse_5 = ll5.split("|")
        hsk5s[parse_5[0]] = [parse_5[1],parse_5[2]]
    zh_dict[5] = hsk5s

    with open("hsk/hsk6", encoding="utf-8") as file:
        val = file.read()
    ll6s = val.split("\n")
    for ll6 in ll6s:
        parse_6 = ll6.split("|")
        hsk6s[parse_6[0]] = [parse_6[1],parse_6[2]]
    zh_dict[6] = hsk6s
    with open("hsk/leftover", encoding="utf-8") as file:
        val = file.read()
    ll7s = val.split("\n")
    for ll7 in ll7s:
        try:
            parse_7 = ll7.split("|")
            hsk7s[parse_7[0]] = [parse_7[1],parse_7[2]]
        except Exception as e:
            pass
    zh_dict[7] = hsk7s

def set_zhuyin_files():
    level1s = {}
    level2s = {}
    level3s = {}
    level4s = {}
    level5s = {}
    level6s = {}
    level7s = {}
    global tw_dict
    with open("chinese_words/level1", encoding="utf-8") as file:
        val = file.read()
    ll1s = val.split("\n")
    for ll1 in ll1s:
        parse_1 = ll1.split("|")
        level1s[parse_1[0]] = [parse_1[1],parse_1[2]]
    tw_dict[1] = level1s
    with open("chinese_words/level2", encoding="utf-8") as file:
        val = file.read()
    ll2s = val.split("\n")

    for ll2 in ll2s:
        parse_2 = ll2.split("|")
        level2s[parse_2[0]] = [parse_2[1], parse_2[2]]
    tw_dict[2] = level2s

    with open("chinese_words/level3", encoding="utf-8") as file:
        val = file.read()
    ll3s = val.split("\n")
    for ll3 in ll3s:
        parse_3 = ll3.split("|")
        level3s[parse_3[0]] = [parse_3[1], parse_3[2]]
    tw_dict[3] = level3s

    with open("chinese_words/level4", encoding="utf-8") as file:
        val = file.read()
    ll4s = val.split("\n")
    for ll4 in ll4s:
        parse_4 = ll4.split("|")
        level4s[parse_4[0]] = [parse_4[1], parse_4[2]]
    tw_dict[4] = level4s

    with open("chinese_words/level5", encoding="utf-8") as file:
        val = file.read()
    ll5s = val.split("\n")
    for ll5 in ll5s:
        parse_5 = ll5.split("|")
        level5s[parse_5[0]] = [parse_5[1], parse_5[2]]
    tw_dict[5] = level5s

    with open("chinese_words/level6", encoding="utf-8") as file:
        val = file.read()
    ll6s = val.split("\n")
    for ll6 in ll6s:
        parse_6 = ll6.split("|")
        level6s[parse_6[0]] = [parse_6[1], parse_6[2]]
    tw_dict[6] = level6s

    with open("chinese_words/leftover", encoding="utf-8") as file:
        val = file.read()
    ll7s = val.split("\n")
    for ll7 in ll7s:
        parse_7 = ll7.split("|")
        try:
            level7s[parse_7[0]] = [parse_7[1], parse_7[2]]
        except Exception as e:
            pass
    tw_dict[7] = level7s

cache = {}

level1s = {}
level2s = {}
level3s = {}
level4s = {}
level5s = {}
level6s = {}
level7s = {}
set_hsk_files()
set_zhuyin_files()
# with open("text_repo/text3", encoding="utf-8") as file:
#     val = file.read()
def reset_cache():
    global cache
    cache = {}

def analyze_text(txt,number_occurrences_minimum,difficulty_minimum, difficulty_maximum):
    global tw_dict
    global zh_dict
    global leftover_zh
    global leftover_tw
    if hanzidentifier.is_simplified(txt) is True:
        current_dict = zh_dict
        language = "zh-CN"
    else:
        current_dict = tw_dict
        language = "zh-TW"
    words = []
    max = 1
    nhsk1 = 0
    nhsk2 = 0
    nhsk3 = 0
    nhsk4 = 0
    nhsk5 = 0
    nhsk6 = 0
    nhsk7 = 0
    list_ban = ["，","。","」 ","！"," ", "？","：","「","」","、","\n","(",")","～","° ","C","°","（","）","》","《", "／", "；", "\r\n", '“', ",","”","”","","[","]","─"]
    print("Start segmentation ...")
    segs = jieba.cut(txt, cut_all=False)
    print("End  segmentation!")
    word_recurence = {}
    size = 0
    print("Start splitting ...")
    for seg in segs:
        # to_bopomofo(seg, tones=False)
        if seg not in list_ban and not seg.isnumeric():
            size+=1
            if seg in word_recurence:
                word_recurence[seg] += 1
                if word_recurence[seg] > max:
                    max = word_recurence[seg]
            else:
                word_recurence[seg] = 1
    print("End splitting!")
    sum = 0
    number_of_words = 0
    translator = Translator()
    print("Start processing ...")
    for key in word_recurence:
        get_words = False
        if key in current_dict[1]:
            hsk_level = 1
            get_words = True
            nhsk1 += word_recurence[key]
            cur_pinyin, translation = current_dict[1][key]
        elif key in current_dict[2]:
            hsk_level = 2
            get_words = True
            nhsk2 += word_recurence[key]
            cur_pinyin, translation = current_dict[2][key]
        elif key in current_dict[3]:
            hsk_level = 3
            get_words = True
            nhsk3 += word_recurence[key]
            cur_pinyin, translation = current_dict[3][key]
        elif key in current_dict[4]:
            hsk_level = 4
            get_words = True
            nhsk4 += word_recurence[key]
            cur_pinyin, translation = current_dict[4][key]
        elif key in current_dict[5]:
            hsk_level = 5
            get_words = True
            nhsk5 += word_recurence[key]
            cur_pinyin, translation = current_dict[5][key]
        elif key in current_dict[6]:
            hsk_level = 6
            get_words = True
            nhsk6 += word_recurence[key]
            cur_pinyin, translation = current_dict[6][key]
        elif key in current_dict[7]:
            hsk_level = 7
            get_words = True
            nhsk7 += word_recurence[key]
            cur_pinyin, translation = current_dict[7][key]
        else:
            nhsk7 += word_recurence[key]
            if language == "zh-CN":
                leftover_zh.add(key)
            else:
                leftover_tw.add(key)
            continue
        if word_recurence[key] >= number_occurrences_minimum and (hsk_level >= difficulty_minimum and hsk_level <= difficulty_maximum):
            number_of_words += 1
            sum += word_recurence[key]
            word = [key,cur_pinyin,translation,hsk_level,word_recurence[key]]
            words.append(word)
    print("End processing!")
    return_object = {
        "basic_info": {
            "filter":{
                "number_occurrences_minimum": number_occurrences_minimum,
                "difficulty_minimum":difficulty_minimum,
                "difficulty_maximum":difficulty_maximum
            },
            "size":size,
            "covered":sum,
            "number_of_words":number_of_words,
        },
        "level_split":{
            "1": nhsk1,
            "2": nhsk2,
            "3": nhsk3,
            "4": nhsk4,
            "5": nhsk5,
            "6": nhsk6,
            "7": nhsk7
        },
        "words": words
    }
    # print("Number of occurrences minimum = {}".format(number_occurrences_minimum))
    # print("Size = {}".format(size))
    # print("Covered = {}".format(sum))
    # print("Percentage covered = {}%".format(int(sum/size*100)))
    # print("Number of words = {}".format(number_of_words))
    return return_object



def write_file():
    global leftover_tw
    global leftover_zh
    print(leftover_tw)
    text = ""
    try:
        for word in leftover_tw:
            print(word)
            text += translate(word,"zh-TW") + '\n'
    except Exception as e:
        print(e)
    with open("chinese_words/leftover", "a", encoding="utf-8") as file:
        file.write(text)
    for word in leftover_zh:
        text += translate(word,"zh-CN") + '\n'
    with open("hsk/leftover", "a", encoding="utf-8") as file:
        file.write(text)
    leftover_tw = set()
    leftover_zh = set()
    return {"result":"yes"}

def translate(word, language):
    try:
        cur_pinyin = pinyin.get(word)
    except Exception as e:
        cur_pinyin = "No pinyin available"
    try:
        translation = translator.translate(word, src=language).text
    except Exception as e:
        # print(e)
        translation = "No translation"
    return "{}|{}|{}".format(word,cur_pinyin,translation)

def get_cache():
    global leftover_zh
    global leftover_tw
    return [list(leftover_zh),list(leftover_tw)]
    